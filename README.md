Wii controller extensions library
=================================
Platform independent way to use Wii controller extensions. You provide the i2c read/write primitives.

Some of the internal i2c message processing was adapted from https://github.com/madhephaestus/WiiChuck, which is a far more extensive suite of Wii extensions.

However, I built this library to completely separate the i2c implementation from the protocol.

Language
--------
C++11 and up (should compile for anything, doesn't use any OS APIs).

Arduino
-------
Checkout/copy this library to your Arduino libraries folder.

How to use it
-------------
Include the library:

```cpp
#include <wiiexti2c.h>
```

Construct the class:

```cpp
WiiExti2c controller;
```

If you want to process data from a Wii classic controller, you will need this class as well
```cpp
WiiExtClassic classic;
```

Tell the library how to do i2c:

```cpp
controller.set_write([](uint8_t addr, const uint8_t *data, uint8_t size) {
    // example using Wire.h
    // Wire.beginTransmission(addr);
    // for (uint8_t i = 0; i < size; i++) {
    //     Wire.write(data[i]);
    // }
    // uint8_t error = Wire.endTransmission();
    // return error == 0;
});
controller.set_read([](uint8_t addr, uint8_t *data, uint8_t size) {
    // example using Wire.h
    // uint8_t recd = Wire.readBytes(data, Wire.requestFrom(addr, size));
    // return recd == size;
});
controller.set_delay_ms([](uint32_t ms) {
    // example using Arduino IDE
    // delay(ms);
});
```

Handle controller data:

```cpp
controller.set_data_callback([](WiiExti2c::WiiExtType type, const uint8_t *data, uint8_t size) {
    if (type == WiiExti2c::WiiExtType::kWiiExtClassic) {
        // for example, tell the Wii classic controller class that you have data for it
        classic.update(data, size);
    }
});
```

Handle Wii classic controller button presses (similar to key-down):

```cpp
classic.set_button_down([](WiiExtButton button) {
    switch(button) {
        case kWiiExtButtonDpadLeft:
            // DPAD left
            break;
        case kWiiExtButtonDpadUp:
            // DPAD up
            break;
        case kWiiExtButtonDpadRight:
            // DPAD right
            break;
        case kWiiExtButtonDpadDown:
            // DPAD down
            break;
    }
});
```

Handle Wii classic controller button de-pressed (similar to key-up):

```cpp
classic.set_button_up([](WiiExtButton button) {
    switch(button) {
        case kWiiExtButtonDpadLeft:
            // DPAD left
            break;
        case kWiiExtButtonDpadUp:
            // DPAD up
            break;
        case kWiiExtButtonDpadRight:
            // DPAD right
            break;
        case kWiiExtButtonDpadDown:
            // DPAD down
            break;
    }
});
```

List of Wii Classic controller button constants:

```cpp
kWiiExtButtonTriggerRight;
kWiiExtButtonPlusStart;
kWiiExtButtonHome;
kWiiExtButtonMinusSelect;
kWiiExtButtonTriggerLeft;
kWiiExtButtonDpadDown;
kWiiExtButtonDpadRight;
kWiiExtButtonDpadUp;
kWiiExtButtonDpadLeft;
kWiiExtButtonZRight;
kWiiExtButtonX;
kWiiExtButtonA;
kWiiExtButtonY;
kWiiExtButtonB;
kWiiExtButtonZLeft;
```

Check for controllers and data by calling the followin in a loop:

```cpp
controller.run_loop();
```
