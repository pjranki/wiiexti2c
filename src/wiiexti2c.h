#ifndef _H_WII_EXT_I2C_H_
#define _H_WII_EXT_I2C_H_

#include <cstdint>
#include <functional>

#ifndef WII_EXT_I2C_DEBUG
#define WII_EXT_I2C_DEBUG(msg)
#endif

const static uint8_t kWiiExtKeyTable0[16]= {0xe0,0x7d,0xe0,0x7d,0xe0,0x7d,0xe0,0x7d,0xe0,0x7d,0x38,0x54,0xbb,0x79,0x01,0x43};
const static uint8_t kWiiExtKeyTable1[16]= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

typedef uint8_t WiiExtButton;
const static WiiExtButton kWiiExtButtonAlwaysOne = 0;
const static WiiExtButton kWiiExtButtonTriggerRight = 1;
const static WiiExtButton kWiiExtButtonPlusStart = 2;
const static WiiExtButton kWiiExtButtonHome = 3;
const static WiiExtButton kWiiExtButtonMinusSelect = 4;
const static WiiExtButton kWiiExtButtonTriggerLeft = 5;
const static WiiExtButton kWiiExtButtonDpadDown = 6;
const static WiiExtButton kWiiExtButtonDpadRight = 7;
const static WiiExtButton kWiiExtButtonDpadUp = 8;
const static WiiExtButton kWiiExtButtonDpadLeft = 9;
const static WiiExtButton kWiiExtButtonZRight = 10;
const static WiiExtButton kWiiExtButtonX = 11;
const static WiiExtButton kWiiExtButtonA = 12;
const static WiiExtButton kWiiExtButtonY = 13;
const static WiiExtButton kWiiExtButtonB = 14;
const static WiiExtButton kWiiExtButtonZLeft = 15;
const static uint8_t kWiiExtButtonCount = 16;

class WiiExtClassic
{
    public:
        typedef std::function<void(WiiExtButton)> ButtonPressFn;

        WiiExtClassic() : button_down_(nullptr), button_up_(nullptr) {
            for (uint8_t i = 0; i < 6; i++) {
                data_[i] = 0xff;
            }
        }
        ~WiiExtClassic() {}
        void set_button_down(ButtonPressFn func) {button_down_ = func;}
        void set_button_up(ButtonPressFn func) {button_up_ = func;}
        void update(const uint8_t *data, uint8_t size) {
            if (size < 6) {
                return;
            }
            size = 6;
            for (WiiExtButton button = 0; button < kWiiExtButtonCount; button++) {
                uint8_t byte = button >= 8 ? 5 : 4;
                uint8_t bit = 1 << (button >= 8 ? button - 8 : button);
                bool prev = (0 != (~data_[byte] & bit));
                bool current = (0 != (~data[byte] & bit));
                if (current != prev) {
                    if (current && button_down_) {
                        button_down_(button);
                    }
                    else if (!current && button_up_) {
                        button_up_(button);
                    }
                }
            }
            copy_data(data_, data, size);
        }
        void copy_data(void *dst, const void *src, uint32_t size) {
            for (uint32_t i = 0; i < size; i++) {
                ((uint8_t*)dst)[i] = ((const uint8_t*)src)[i];
            }
        }
    private:
        ButtonPressFn button_down_;
        ButtonPressFn button_up_;
        uint8_t data_[6];
};

class WiiExti2c
{
    public:
        const static uint8_t kWiiExti2cAddr = 0x52;

        enum WiiExtType {
            kWiiExtUnknown,
            kWiiExtNunchuck,
            kWiiExtClassic,
            kWiiExtGuitarHeroController,
            kWiiExtGuitarHeroWorldTourDrums,
            kWiiExtDrumController,
            kWiiExtDrawsomeTablet,
            kWiiExtTurntable,
        };

        WiiExti2c() :
            write_(nullptr),
            read_(nullptr),
            delay_ms_(nullptr),
            data_callback_(nullptr),
            encrypted_(false),
            connected_(false),
            type_(kWiiExtUnknown) {}
        ~WiiExti2c() {}

        typedef std::function<bool(uint8_t,const uint8_t*, uint8_t)> WriteFn;
        typedef std::function<bool(uint8_t,uint8_t*, uint8_t)> ReadFn;
        typedef std::function<void(uint32_t)> DelayFn;
        typedef std::function<void(WiiExtType, const uint8_t *, uint8_t)> DataCallbackFn;

        void set_write(WriteFn func) {write_ = func;}
        void set_read(ReadFn func) {read_ = func;}
        void set_delay_ms(DelayFn func) {delay_ms_ = func;}
        void set_data_callback(DataCallbackFn func) {data_callback_ = func;}

        void encryption(bool enc) {
            encrypted_ = enc;
        }
        bool connect() {
            connected_ = false;
            if (!write_init_bytes()) {
                WII_EXT_I2C_DEBUG("connect failed to write init bytes");
                return false;
            }
            uint8_t data[6] = {0};
            delay_ms(100);
            if (!read_address(0x00, data, 6)) {
                WII_EXT_I2C_DEBUG("connect failed to read first 6 bytes");
                return false;
            }
            delay_ms(100);
            if (!read_address(0x00, data, 6)) {
                WII_EXT_I2C_DEBUG("connect failed to read second 6 bytes");
                return false;
            }
            WII_EXT_I2C_DEBUG("CONNECTED");
            connected_ = true;
            return true;
        }
        bool connected() {
            return connected_;
        }
        bool disconnected() {
            return !connected_;
        }
        bool controller_type() {
            return type_;
        }
        bool read_data(uint8_t *data, uint32_t size) {
            uint8_t temp[6] = {0};
            if (!read_address(0, temp, 6)) {
                WII_EXT_I2C_DEBUG("failed to read controller data, assuming disconnected");
                WII_EXT_I2C_DEBUG("DISCONNECTED");
                connected_ = false;
                return false;
            }
            copy_data(data, temp, size < 6 ? size : 6);
            return true;
        }
        void run_loop() {
            // Connect to controller
            if (disconnected()) {
                connect();
            }

            // Read data from controller
            if(connected()) {
                uint8_t data[6] = {0};
                if (read_data(data, 6)) {
                    if (data_callback_) {
                        data_callback_(type_, data, 6);
                    }
                }
            }
        }

        bool write_init_bytes() {
            WII_EXT_I2C_DEBUG("init wii extension");
            if (!write_register(0xf0, 0x55)) {
                WII_EXT_I2C_DEBUG("error writing init bytes (1)");
                return false;
            }
            if (!write_register(0xfb, 0x00)) {
                WII_EXT_I2C_DEBUG("error writing init bytes (1)");
                return false;
            }
            delay_ms(100);
            type_ = identify_controller();
            if (type_ == kWiiExtUnknown) {
                WII_EXT_I2C_DEBUG("error identifying controller or controler type unknown");
                return false;
            }
            delay_ms(100);
            if (encrypted_) {
                WII_EXT_I2C_DEBUG("Beginning Encrypted Comms");

                delay_ms(100);

                if (!write_register(0xF0, 0xAA)) { // enable enc mode?
                    WII_EXT_I2C_DEBUG("error enabling encryption mode");
                    return false;
                }
                delay_ms(90);

                if (!write_address(0x40, kWiiExtKeyTable1, 8)) {
                    WII_EXT_I2C_DEBUG("error writing key table (1 of 2)");
                    return false;
                }
                if (!write_address(0x48, kWiiExtKeyTable1 + 8, 8)) {
                    WII_EXT_I2C_DEBUG("error writing key table (2 of 2)");
                    return false;
                }
                delay_ms(100);

                //write_register(0x40, 0x00);
            }
            return true;
        }
        WiiExtType identify_controller() {
            uint8_t data[6] = {0};
            if (!read_address(0xfa, data, 6)) {
                WII_EXT_I2C_DEBUG("error reading controller type data");
                return kWiiExtUnknown;
            }
            return wii_ext_type_from_data(data);
        }
        bool write_address(uint8_t addr, const uint8_t *data, uint8_t size) {
            uint8_t request[9] = {0};
            if (size > (sizeof(request) - 1)) {
                WII_EXT_I2C_DEBUG("write address data array too small, make bigger");
                return false;
            }
            request[0] == addr;
            copy_data(request + 1, data, size);
            if (!write(request, size + 1)) {
                WII_EXT_I2C_DEBUG("error writing address with data");
                return false;
            }
            return true;
        }
        bool read_address(uint8_t addr, uint8_t *data, uint8_t size) {
            // send conversion command
            if (!write(&addr, 1)) {
                WII_EXT_I2C_DEBUG("error writing addr when trying to read data");
                return false;
            }

            // wait for data to be converted
            if (!delay_ms(1)) {
                WII_EXT_I2C_DEBUG("error waiting for data to be converted when trying to read data");
                return false;
            }

            // read data
            if (!read(data, size)) {
                WII_EXT_I2C_DEBUG("error reading data when trying to read data");
                return false;
            }

            // decrypt
            if (encrypted_) {
                for (uint8_t i = 0; i < size; i++) {
                    data[i] = decrypt_byte(data[i], addr + i);
                }
            }

            // done
            return true;
        }
        bool write_register(uint8_t reg, uint8_t value) {
            uint8_t data[2] = {reg, value};
            if (!write(data, 2)) {
                WII_EXT_I2C_DEBUG("error writing register");
                return false;
            }
            return true;
        }

        uint8_t decrypt_byte(uint8_t byte, uint8_t address) {
            //return (byte ^ _key_table_1[address % 8]) + _key_table_1[(address % 8)+0x08];
            return (byte ^ 0x97) + 0x97;
        }

        bool write(const uint8_t *data, uint8_t size) {
            if (write_ == nullptr) {
                WII_EXT_I2C_DEBUG("no write function provided");
                return false;
            }
            return write_(kWiiExti2cAddr, data, size);
        }
        bool read(uint8_t *data, uint32_t size) {
            if (read_ == nullptr) {
                WII_EXT_I2C_DEBUG("no read function provided");
                return false;
            }
            return read_(kWiiExti2cAddr, data, size);
        }
        bool delay_ms(uint32_t ms) {
            if (delay_ms_ == nullptr) {
                WII_EXT_I2C_DEBUG("no delay (ms) function provided");
            }
            delay_ms_(ms);
            return true;
        }

        WiiExtType wii_ext_type_from_data(const uint8_t *data) {
            if (is_data_equal(data + 4, "\x00\x00", 2)) {
                WII_EXT_I2C_DEBUG("Nunchuck");
                return kWiiExtNunchuck;  // nunchuck
            } else if (is_data_equal(data + 4, "\x01\x01", 2)) {
                WII_EXT_I2C_DEBUG("Wii Classic");
                return kWiiExtClassic;  // Classic Controller
            } else if (is_data_equal(data, "\x00\x00\xa4\x20\x01\x03", 6)) {
                WII_EXT_I2C_DEBUG("Guitar Hero Controller");
                return kWiiExtGuitarHeroController;  // Guitar Hero Controller
            } else if (is_data_equal(data, "\x01\x00\xa4\x20\x01\x03", 6)) {
                WII_EXT_I2C_DEBUG("Guitar Hero World Tour Drums");
                return kWiiExtGuitarHeroWorldTourDrums;  // Guitar Hero World Tour Drums
            } else if (is_data_equal(data, "\x03\x00\xa4\x20\x01\x03", 6)) {
                WII_EXT_I2C_DEBUG("Turntable");
                return kWiiExtTurntable;
            } else if (is_data_equal(data, "\x00\x00\xa4\x20\x01\x11", 6)) {
                WII_EXT_I2C_DEBUG("Taiko no Tatsujin TaTaCon (Drum controller)");
                return kWiiExtDrumController;  // Taiko no Tatsujin TaTaCon (Drum controller)
            } else if (is_data_equal(data, "\xff\x00\xa4\x20\x00\x13", 6)) {
                WII_EXT_I2C_DEBUG("Drawsome Tablet");
                return kWiiExtDrawsomeTablet;  // Drawsome Tablet
            }
            return kWiiExtUnknown;
        }
        bool is_data_equal(const void *d1, const void *d2, uint32_t size) {
            for (uint32_t i; i < size; i++) {
                if (((const uint8_t*)d1)[i] != ((const uint8_t*)d2)[i]) {
                    return false;
                }
            }
            return true;
        }
        void copy_data(void *dst, const void *src, uint32_t size) {
            for (uint32_t i = 0; i < size; i++) {
                ((uint8_t*)dst)[i] = ((const uint8_t*)src)[i];
            }
        }
    private:
        WriteFn write_;
        ReadFn read_;
        DelayFn delay_ms_;
        DataCallbackFn data_callback_;
        bool encrypted_;
        bool connected_;
        WiiExtType type_;
};

#endif
